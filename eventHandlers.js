'use strict';

let registerEventHandlers = (eventHandlers, skillContext) => {
    eventHandlers.onSessionStarted = (sessionStartedRequest, session) => {
        skillContext.needMoreHelp = false;
    };

    eventHandlers.onLaunch = (launchRequest, session, response) => {
        response.ask('Hi there! Reekoh skill is now ready! You can get the latest reekoh plugins, latest reekoh blog posts or even send data to your reekoh instance. What do you want to do?', 'You can for reekohs latest plugins or blog posts or even send data to your reekoh instance.');
    };
};

exports.register = registerEventHandlers;